package com.example.android.ridoh_1202160222_si4005_pab_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity {

    TextView viewTujuan, viewTglBerangkat, viewWaktuBerangkat,
            viewTglPulang, viewWaktuPulang, viewJumlahTiket, viewHargaTotal;

    Button konfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        viewTujuan = (TextView)findViewById(R.id.viewTujuan);
        viewTglBerangkat = (TextView)findViewById(R.id.viewTglBerangkat);
        viewWaktuBerangkat = (TextView)findViewById(R.id.viewWaktuBerangkat);
        viewTglPulang = (TextView)findViewById(R.id.viewTglPulang);
        viewWaktuPulang= (TextView)findViewById(R.id.viewWaktuPulang);
        viewJumlahTiket = (TextView)findViewById(R.id.viewJumlahTiket);
        viewHargaTotal = (TextView)findViewById(R.id.viewHargaTotal);
        konfirmasi = (Button)findViewById(R.id.btnKonfirmasi);

        Intent in = getIntent();

        String tujuan = in.getStringExtra("daerahTujuan");
        String tanggalb = in.getStringExtra("tanggalBerangkat");
        String waktub = in.getStringExtra("waktuBerangkat");
        String tanggalp = in.getStringExtra("tanggalPulang");
        String waktup = in.getStringExtra("waktuPulang");
        String jumlahTiket = in.getStringExtra("jumlahTiket");
        String hargaTotal = in.getStringExtra("hargaTotal");

        viewTujuan.setText(tujuan);
        viewTglBerangkat.setText(tanggalb);
        viewWaktuBerangkat.setText(waktub);
        viewTglPulang.setText(tanggalp);
        viewWaktuPulang.setText(waktup);
        viewJumlahTiket.setText(jumlahTiket);
        viewHargaTotal.setText(hargaTotal);

        konfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Selamat Menikmati Perjalanan Anda...!!!", Toast.LENGTH_LONG).show();
                finish();
                moveTaskToBack(true);
            }
        });
    }
}
