package com.example.android.ridoh_1202160222_si4005_pab_modul2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Main2Activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    final Context context = this;
    private Button btnTopUp, buttonDatePicker, buttonTimePicker, buttonDatePickerPulang, buttonTimePickerPulang, buttonBeli;
    private EditText result, jumlahTiket;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener2;
    private DatePickerDialog.OnDateSetListener mDateSetListener2;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Switch switchPulangPergi;
    TextView saldo;
    private Spinner spinnerTujuan;
    private static final String[] detailTujuan = {"Jakarta (Rp. 85.000)", "Cirebon (Rp.150.000)", "Bekasi (Rp.70.000)"};
    String alamatTujuan;
    private int hargaTiket;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

// components from main.xml
        btnTopUp = (Button) findViewById(R.id.btnTop_Up);

        buttonDatePicker = (Button) findViewById(R.id.btnTglPergi);
        buttonTimePicker = (Button) findViewById(R.id.btnWaktuPergi);
        buttonDatePickerPulang = (Button) findViewById(R.id.btnTglPulang);
        buttonTimePickerPulang = (Button) findViewById(R.id.btnWaktuPulang);
        switchPulangPergi = (Switch) findViewById(R.id.switchPulangPergi);
        spinnerTujuan = (Spinner) findViewById(R.id.spinnerTujuan);
        saldo = findViewById(R.id.saldo);
        jumlahTiket = (EditText) findViewById(R.id.jumlahTiket);
        buttonBeli = (Button) findViewById(R.id.btnBeli);

//        buttonBeli = (Button)findViewById(R.id.btnBeli) ;
//        jumlahTiket = (EditText)findViewById(R.id.jumlahTiket);


        //===============================================TOP UP=======================================================//
        // add button listener
        btnTopUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // get top_up.xml view
                LayoutInflater li = LayoutInflater.from(context);
                final View top_upView = li.inflate(R.layout.top_up, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set top_up.xml to alertdialog builder
                alertDialogBuilder.setView(top_upView);

                final EditText userInput = (EditText) top_upView
                        .findViewById(R.id.masukkan_saldo);

                // set dialog message
                alertDialogBuilder
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        // edit text

                                        result = (EditText) top_upView.findViewById(R.id.masukkan_saldo);
                                        int tambahSaldo = Integer.parseInt(saldo.getText().toString()) + Integer.parseInt(result.getText().toString());
                                        saldo.setText(tambahSaldo + "");
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        //===================================================DATE & TIME ===========================================//

        buttonDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                mYear = cal.get(Calendar.YEAR);
                mMonth = cal.get(Calendar.MONTH);
                mDay = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dateDialog = new DatePickerDialog(Main2Activity.this,
                        mDateSetListener,
                        mYear, mMonth, mDay);
                dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dateDialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = mMonth + 1;
                String date = dayOfMonth + "-" + month + "-" + year;
                buttonDatePicker.setText(date);
            }
        };

        buttonTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                mHour = cal.get(Calendar.HOUR_OF_DAY);
                mMinute = cal.get(Calendar.MINUTE);

                TimePickerDialog timeDialog = new TimePickerDialog(Main2Activity.this,
                        mTimeSetListener,
                        mHour, mMinute, false);
                timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog.show();
            }
        });

        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time = hourOfDay + " : " + minute;
                buttonTimePicker.setText(time);
            }
        };

        //======================================SWITCH============================================================//
        switchPulangPergi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (switchPulangPergi.isChecked() == true) {

                    buttonDatePickerPulang.setVisibility(View.VISIBLE);
                    buttonTimePickerPulang.setVisibility(View.VISIBLE);

                    buttonDatePickerPulang.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mYear = cal.get(Calendar.YEAR);
                            mMonth = cal.get(Calendar.MONTH);
                            mDay = cal.get(Calendar.DAY_OF_MONTH);

                            DatePickerDialog dateDialog = new DatePickerDialog(Main2Activity.this,
                                    mDateSetListener2,
                                    mYear, mMonth, mDay);
                            dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                            dateDialog.show();
                        }
                    });

                    mDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month = mMonth + 1;
                            String date = dayOfMonth + "-" + month + "-" + year;
                            buttonDatePickerPulang.setText(date);
                        }
                    };

                    buttonTimePickerPulang.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mHour = cal.get(Calendar.HOUR_OF_DAY);
                            mMinute = cal.get(Calendar.MINUTE);

                            TimePickerDialog timeDialog = new TimePickerDialog(Main2Activity.this,
                                    mTimeSetListener2,
                                    mHour, mMinute, false);
                            timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            timeDialog.show();
                        }
                    });

                    mTimeSetListener2 = new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String time = hourOfDay + " : " + minute;
                            buttonTimePickerPulang.setText(time);
                        }
                    };

                } else if (!switchPulangPergi.isChecked()) {
                    buttonDatePickerPulang.setVisibility(View.GONE);
                    buttonTimePickerPulang.setVisibility(View.GONE);

                    buttonDatePickerPulang.setText("PILIH TANGGAL");
                    buttonTimePickerPulang.setText("PILIH WAKTU");

                    buttonDatePicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mYear = cal.get(Calendar.YEAR);
                            mMonth = cal.get(Calendar.MONTH);
                            mDay = cal.get(Calendar.DAY_OF_MONTH);

                            DatePickerDialog dateDialog = new DatePickerDialog(Main2Activity.this,
                                    mDateSetListener,
                                    mYear, mMonth, mDay);
                            dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                            dateDialog.show();
                        }
                    });

                    mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month = mMonth + 1;
                            String date = dayOfMonth + "-" + month + "-" + year;
                            buttonDatePicker.setText(date);
                        }
                    };

                    buttonTimePicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mHour = cal.get(Calendar.HOUR_OF_DAY);
                            mMinute = cal.get(Calendar.MINUTE);

                            TimePickerDialog timeDialog = new TimePickerDialog(Main2Activity.this,
                                    mTimeSetListener,
                                    mHour, mMinute, false);
                            timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            timeDialog.show();
                        }
                    });

                    mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String time = hourOfDay + " : " + minute;
                            buttonTimePicker.setText(time);
                        }
                    };
                }
            }
        });

        //=============================================================SPINER==============================================================================//

        spinnerTujuan = findViewById(R.id.spinnerTujuan);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Main2Activity.this, android.R.layout.simple_spinner_item, detailTujuan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTujuan.setAdapter(adapter);
        spinnerTujuan.setOnItemSelectedListener(this);

        //==============================================================BUTTON BELI ============================================================================//

        buttonBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer a = 1;
                Integer hargaTotal = 0;
                Integer totalSaldo = Integer.parseInt(saldo.getText().toString());
                Integer totalTiket = Integer.parseInt(jumlahTiket.getText().toString());

                hargaTotal = a*hargaTiket*totalTiket;
                Integer sisaSaldo = totalSaldo - (hargaTotal);


                Intent intent = new Intent(Main2Activity.this, Main3Activity.class);
                intent.putExtra("daerahTujuan", alamatTujuan);
                intent.putExtra("tanggalBerangkat", buttonDatePicker.getText().toString());
                intent.putExtra("waktuBerangkat", buttonTimePicker.getText().toString());
                intent.putExtra("tanggalPulang", buttonDatePickerPulang.getText().toString());
                intent.putExtra("waktuPulang", buttonTimePickerPulang.getText().toString());
                intent.putExtra("jumlahTiket",  jumlahTiket.getText().toString());
                intent.putExtra("hargaTotal", hargaTotal.toString());
                intent.putExtra("saldo", saldo.getText().toString());
                intent.putExtra("sisaSaldo", sisaSaldo.toString());
                Main2Activity.this.startActivity(intent);
            }
            });
        }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    switch (position){
        case 0 :
     alamatTujuan = "Jakarta";
     hargaTiket = 85000;
     break;
        case 1 :
     alamatTujuan = "Cirebon";
     hargaTiket = 150000;
     break;
        case 2 :
     alamatTujuan = "Bekasi";
     hargaTiket = 70000;
     break;
    }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
